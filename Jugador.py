import random
from re import X 

class jugador:
    
    def realizarJugada(self):
        
        simbolo=self.seleccionaJugador()
        
        if simbolo=='X':
            self.jugadorComputador()
        else:
            self.jugadorUsuario()
    
    def seleccionaJugador(self,simbolo):
        if simbolo=='X':
            simbolo='O'
        else:
            simbolo='x'
        return simbolo
    
    def jugadaUsuario(self,tablero,simbolo):
        terminar=False 
        while not terminar:
            x=int(input("Digite la x:"))
            y=int(input("Digite la y:"))
        
        if not(tablero[x][y]=='O' or tablero [x][y]=='X'):
            terminar=True
        tablero[x][y]=simbolo
        return tablero
    
    
class jugadorPc:
    def JugarComputador(self,tablero,simbolo):
        terminar=False
        while not terminar:
            x=random.randint(0,2)
            y=random.randint(0,2)
            
        if not (tablero[x][y]=='o' or tablero[x][y]=='x'):
            terminar=False
        tablero[x][y]=simbolo
        return tablero
